#include <stdint.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	if (argc != 3)
		return (0);
	uint32_t num1;
	uint32_t num2;
	uint32_t result;
	uint32_t oprend;
	
	FILE * fn1 = fopen(argv[1], "r+");
	FILE * fn2 = fopen(argv[2], "r+");
	fread(&num1, sizeof(uint32_t), 1, fn1);
	fread(&num2, sizeof(uint32_t), 1, fn2);
	
	oprend = ((num1&0xff)<<24 |(num1&0xff00)<<8|(num1&0xff0000)>>8|(num1&0xff000000)>>24);
	num1 = oprend;
	
	oprend = ((num2&0xff)<<24 |(num2&0xff00)<<8|(num2&0xff0000)>>8|(num2&0xff000000)>>24);
	num2 = oprend;

	result = num1 + num2;
	printf("%u(0x%x) + %u(0x%x) = %u(0x%x)\n", num1, num1, num2, num2, result, result);
	return (0);
}
