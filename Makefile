#Makefile
all: add-nbo

add-nbo: main.o
	gcc -o add-nbo main.o

main.o: main.c
	gcc -c main.c

clean:
	rm -f add-nbo
	rm -f *.o
